package com.aiknights.com.rapidviewmain.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Detection {
	

	private List<Integer> box;
	
	@JsonProperty("center_closeness")
	private Double centerCloseness;
	
	@JsonProperty("class")
	private String classs;
	
	@JsonProperty("damage_max_shingle_min_ratio")
	private Double damageMaxShingleMinRatio;
	
	@JsonProperty("damage_min_shingle_min_ratio")
	private Double damageMinShingleMinRatio;
	
	private Integer id;
	private Integer image_heigt;
	private Integer image_width;
	private List<List<Integer>> polygon;
	private Double score;
	private Integer radius;
	
	@JsonProperty("shingle_max")
	private Integer shingleMax;
	@JsonProperty("shingle_min")
	private Integer shingleMin;
	
	
	public Detection() {
		super();
	}

	public List<Integer> getBox() {
		return box;
	}

	public void setBox(List<Integer> box) {
		this.box = box;
	}

	public String getClasss() {
		return classs;
	}

	public void setClasss(String classs) {
		this.classs = classs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getImage_heigt() {
		return image_heigt;
	}

	public void setImage_heigt(Integer image_heigt) {
		this.image_heigt = image_heigt;
	}

	public Integer getImage_width() {
		return image_width;
	}

	public void setImage_width(Integer image_width) {
		this.image_width = image_width;
	}

	public List<List<Integer>> getPolygon() {
		return polygon;
	}

	public void setPolygon(List<List<Integer>> polygon) {
		this.polygon = polygon;
	}

	public Integer getRadius() {
		return radius;
	}

	public void setRadius(Integer radius) {
		this.radius = radius;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Double getCenterCloseness() {
		return centerCloseness;
	}

	public void setCenterCloseness(Double centerCloseness) {
		this.centerCloseness = centerCloseness;
	}

	public Double getDamageMaxShingleMinRatio() {
		return damageMaxShingleMinRatio;
	}

	public void setDamageMaxShingleMinRatio(Double damageMaxShingleMinRatio) {
		this.damageMaxShingleMinRatio = damageMaxShingleMinRatio;
	}

	public Double getDamageMinShingleMinRatio() {
		return damageMinShingleMinRatio;
	}

	public void setDamageMinShingleMinRatio(Double damageMinShingleMinRatio) {
		this.damageMinShingleMinRatio = damageMinShingleMinRatio;
	}

	public Integer getShingleMax() {
		return shingleMax;
	}

	public void setShingleMax(Integer shingleMax) {
		this.shingleMax = shingleMax;
	}

	public Integer getShingleMin() {
		return shingleMin;
	}

	public void setShingleMin(Integer shingleMin) {
		this.shingleMin = shingleMin;
	}

	@Override
	public String toString() {
		return "Detection [box=" + box + ", centerCloseness=" + centerCloseness + ", classs=" + classs
				+ ", damageMaxShingleMinRatio=" + damageMaxShingleMinRatio + ", damageMinShingleMinRatio="
				+ damageMinShingleMinRatio + ", id=" + id + ", image_heigt=" + image_heigt + ", image_width="
				+ image_width + ", polygon=" + polygon + ", score=" + score + ", radius=" + radius + ", shingleMax="
				+ shingleMax + ", shingleMin=" + shingleMin + "]";
	}


	
	
	
	
	

}
