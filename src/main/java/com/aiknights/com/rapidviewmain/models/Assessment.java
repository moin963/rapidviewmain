package com.aiknights.com.rapidviewmain.models;

import java.util.Map;

public class Assessment {

	private Map<String,Object> blurriness;
	private String name;
	private Map<String,Object> scale;
	private String status;
	
	public Assessment() {
		super();
	}

	public Assessment(Map<String, Object> blurriness, String name, Map<String, Object> scale, String status) {
		super();
		this.blurriness = blurriness;
		this.name = name;
		this.scale = scale;
		this.status = status;
	}

	public Map<String, Object> getBlurriness() {
		return blurriness;
	}

	public void setBlurriness(Map<String, Object> blurriness) {
		this.blurriness = blurriness;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, Object> getScale() {
		return scale;
	}

	public void setScale(Map<String, Object> scale) {
		this.scale = scale;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Assessment [blurriness=" + blurriness + ", name=" + name + ", scale=" + scale + ", status=" + status
				+ "]";
	}
	
	

	
	
	
	
	
}
