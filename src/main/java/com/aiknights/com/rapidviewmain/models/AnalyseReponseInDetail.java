package com.aiknights.com.rapidviewmain.models;

public class AnalyseReponseInDetail {
	
	private long sourceImageId;
	private String sourceImageUrl;
	private String sourceImageName;
	private String analysedImageUrl;
	private String analysedImageId;
	private String assessmentBlurrinessError;
	private String imageName;
	private String assessmentScaleError;
	private String assessmentStatus;
    private ClassType classType;
    private double detectionScore;
    private double damageSize;
    private String shingleType;
    
    
	public AnalyseReponseInDetail() {
		super();
	}

	public AnalyseReponseInDetail(long sourceImageId,String sourceImageUrl,String sourceImageName) {
		super();
		this.sourceImageId = sourceImageId;
		this.sourceImageUrl = sourceImageUrl;
		this.sourceImageName = sourceImageName;
	}

	public long getSourceImageId() {
		return sourceImageId;
	}

	public void setSourceImageId(long sourceImageId) {
		this.sourceImageId = sourceImageId;
	}

	public String getSourceImageUrl() {
		return sourceImageUrl;
	}

	public void setSourceImageUrl(String sourceImageUrl) {
		this.sourceImageUrl = sourceImageUrl;
	}

	public String getSourceImageName() {
		return sourceImageName;
	}

	public void setSourceImageName(String sourceImageName) {
		this.sourceImageName = sourceImageName;
	}

	public String getAnalysedImageUrl() {
		return analysedImageUrl;
	}

	public void setAnalysedImageUrl(String analysedImageUrl) {
		this.analysedImageUrl = analysedImageUrl;
	}

	public String getAnalysedImageId() {
		return analysedImageId;
	}

	public void setAnalysedImageId(String analysedImageId) {
		this.analysedImageId = analysedImageId;
	}

	public String getAssessmentBlurrinessError() {
		return assessmentBlurrinessError;
	}

	public void setAssessmentBlurrinessError(String assessmentBlurrinessError) {
		this.assessmentBlurrinessError = assessmentBlurrinessError;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getAssessmentScaleError() {
		return assessmentScaleError;
	}

	public void setAssessmentScaleError(String assessmentScaleError) {
		this.assessmentScaleError = assessmentScaleError;
	}

	public String getAssessmentStatus() {
		return assessmentStatus;
	}

	public void setAssessmentStatus(String assessmentStatus) {
		this.assessmentStatus = assessmentStatus;
	}

	public ClassType getClassType() {
		return classType;
	}

	public void setClassType(ClassType classType) {
		this.classType = classType;
	}

	public double getDetectionScore() {
		return detectionScore;
	}

	public void setDetectionScore(double detectionScore) {
		this.detectionScore = detectionScore;
	}

	public double getDamageSize() {
		return damageSize;
	}

	public void setDamageSize(double damageSize) {
		this.damageSize = damageSize;
	}

	public String getShingleType() {
		return shingleType;
	}

	public void setShingleType(String shingleType) {
		this.shingleType = shingleType;
	}

	@Override
	public String toString() {
		return "AnalyseReponseInDetail [sourceImageId=" + sourceImageId + ", sourceImageUrl=" + sourceImageUrl
				+ ", sourceImageName=" + sourceImageName + ", analysedImageUrl=" + analysedImageUrl
				+ ", analysedImageId=" + analysedImageId + ", assessmentBlurrinessError=" + assessmentBlurrinessError
				+ ", imageName=" + imageName + ", assessmentScaleError=" + assessmentScaleError + ", assessmentStatus="
				+ assessmentStatus + ", classType=" + classType + ", detectionScore=" + detectionScore + ", damageSize="
				+ damageSize + ", shingleType=" + shingleType + "]";
	}

	




    
    

}
