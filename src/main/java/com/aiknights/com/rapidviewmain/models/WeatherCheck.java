package com.aiknights.com.rapidviewmain.models;

public class WeatherCheck {
	
	private int weatherId;
	private String claimId;
	private String heatMap;
	private String mapDate;
	private String homeOrientation;
	private String weatherAnomolies;
	private double hailSize;
	private int windGusts;
	private String windDirection;
	private String roofSlope;
	private String chatter;
	private double impactFront;
	private double impactLeft;
	private double impactBack;
	private double impactRight;
	private double avgHighTemp;
	private double avgLowTemp;
	private double avgTemp;
	private double  avgRainfall;
	private int avgDaysSine; 
	private int avgSnowfall;
	
	public WeatherCheck() {
		super();
	}

	public WeatherCheck(int weatherId, String claimId, String heatMap, String mapDate, String homeOrientation,
			String weatherAnomolies, double hailSize, int windGusts, String windDirection, String roofSlope,
			String chatter, double impactFront, double impactLeft, double impactBack, double impactRight,
			double avgHighTemp, double avgLowTemp, double avgTemp, double avgRainfall, int avgDaysSine,
			int avgSnowfall) {
		super();
		this.weatherId = weatherId;
		this.claimId = claimId;
		this.heatMap = heatMap;
		this.mapDate = mapDate;
		this.homeOrientation = homeOrientation;
		this.weatherAnomolies = weatherAnomolies;
		this.hailSize = hailSize;
		this.windGusts = windGusts;
		this.windDirection = windDirection;
		this.roofSlope = roofSlope;
		this.chatter = chatter;
		this.impactFront = impactFront;
		this.impactLeft = impactLeft;
		this.impactBack = impactBack;
		this.impactRight = impactRight;
		this.avgHighTemp = avgHighTemp;
		this.avgLowTemp = avgLowTemp;
		this.avgTemp = avgTemp;
		this.avgRainfall = avgRainfall;
		this.avgDaysSine = avgDaysSine;
		this.avgSnowfall = avgSnowfall;
	}

	public int getWeatherId() {
		return weatherId;
	}

	public void setWeatherId(int weatherId) {
		this.weatherId = weatherId;
	}

	public String getClaimId() {
		return claimId;
	}

	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}

	public String getHeatMap() {
		return heatMap;
	}

	public void setHeatMap(String heatMap) {
		this.heatMap = heatMap;
	}

	public String getMapDate() {
		return mapDate;
	}

	public void setMapDate(String mapDate) {
		this.mapDate = mapDate;
	}

	public String getHomeOrientation() {
		return homeOrientation;
	}

	public void setHomeOrientation(String homeOrientation) {
		this.homeOrientation = homeOrientation;
	}

	public String getWeatherAnomolies() {
		return weatherAnomolies;
	}

	public void setWeatherAnomolies(String weatherAnomolies) {
		this.weatherAnomolies = weatherAnomolies;
	}

	public double getHailSize() {
		return hailSize;
	}

	public void setHailSize(double hailSize) {
		this.hailSize = hailSize;
	}

	public int getWindGusts() {
		return windGusts;
	}

	public void setWindGusts(int windGusts) {
		this.windGusts = windGusts;
	}

	public String getWindDirection() {
		return windDirection;
	}

	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;
	}

	public String getRoofSlope() {
		return roofSlope;
	}

	public void setRoofSlope(String roofSlope) {
		this.roofSlope = roofSlope;
	}

	public String getChatter() {
		return chatter;
	}

	public void setChatter(String chatter) {
		this.chatter = chatter;
	}

	public double getImpactFront() {
		return impactFront;
	}

	public void setImpactFront(double impactFront) {
		this.impactFront = impactFront;
	}

	public double getImpactLeft() {
		return impactLeft;
	}

	public void setImpactLeft(double impactLeft) {
		this.impactLeft = impactLeft;
	}

	public double getImpactBack() {
		return impactBack;
	}

	public void setImpactBack(double impactBack) {
		this.impactBack = impactBack;
	}

	public double getImpactRight() {
		return impactRight;
	}

	public void setImpactRight(double impactRight) {
		this.impactRight = impactRight;
	}

	public double getAvgHighTemp() {
		return avgHighTemp;
	}

	public void setAvgHighTemp(double avgHighTemp) {
		this.avgHighTemp = avgHighTemp;
	}

	public double getAvgLowTemp() {
		return avgLowTemp;
	}

	public void setAvgLowTemp(double avgLowTemp) {
		this.avgLowTemp = avgLowTemp;
	}

	public double getAvgTemp() {
		return avgTemp;
	}

	public void setAvgTemp(double avgTemp) {
		this.avgTemp = avgTemp;
	}

	public double getAvgRainfall() {
		return avgRainfall;
	}

	public void setAvgRainfall(double avgRainfall) {
		this.avgRainfall = avgRainfall;
	}

	public int getAvgDaysSine() {
		return avgDaysSine;
	}

	public void setAvgDaysSine(int avgDaysSine) {
		this.avgDaysSine = avgDaysSine;
	}

	public int getAvgSnowfall() {
		return avgSnowfall;
	}

	public void setAvgSnowfall(int avgSnowfall) {
		this.avgSnowfall = avgSnowfall;
	}

	@Override
	public String toString() {
		return "WeatherCheck [weatherId=" + weatherId + ", claimId=" + claimId + ", heatMap=" + heatMap + ", mapDate="
				+ mapDate + ", homeOrientation=" + homeOrientation + ", weatherAnomolies=" + weatherAnomolies
				+ ", hailSize=" + hailSize + ", windGusts=" + windGusts + ", windDirection=" + windDirection
				+ ", roofSlope=" + roofSlope + ", chatter=" + chatter + ", impactFront=" + impactFront + ", impactLeft="
				+ impactLeft + ", impactBack=" + impactBack + ", impactRight=" + impactRight + ", avgHighTemp="
				+ avgHighTemp + ", avgLowTemp=" + avgLowTemp + ", avgTemp=" + avgTemp + ", avgRainfall=" + avgRainfall
				+ ", avgDaysSine=" + avgDaysSine + ", avgSnowfall=" + avgSnowfall + "]";
	}
	
	
	

}
