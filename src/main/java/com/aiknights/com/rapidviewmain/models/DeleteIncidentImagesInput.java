package com.aiknights.com.rapidviewmain.models;

public class DeleteIncidentImagesInput {
	
	private long sourceImageId;
	private String sourceImageUrl;
	private String analysedImageUrl;
	
	public DeleteIncidentImagesInput() {
		super();
	}

	public long getSourceImageId() {
		return sourceImageId;
	}

	public void setSourceImageId(long sourceImageId) {
		this.sourceImageId = sourceImageId;
	}

	public String getSourceImageUrl() {
		return sourceImageUrl;
	}

	public void setSourceImageUrl(String sourceImageUrl) {
		this.sourceImageUrl = sourceImageUrl;
	}

	public String getAnalysedImageUrl() {
		return analysedImageUrl;
	}

	public void setAnalysedImageUrl(String analysedImageUrl) {
		this.analysedImageUrl = analysedImageUrl;
	}

	@Override
	public String toString() {
		return "DeleteIncidentImagesInput [sourceImageId=" + sourceImageId + ", sourceImageUrl=" + sourceImageUrl
				+ ", analysedImageUrl=" + analysedImageUrl + "]";
	}
	
	
	
}
