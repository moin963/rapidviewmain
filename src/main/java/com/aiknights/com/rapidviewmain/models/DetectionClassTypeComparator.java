package com.aiknights.com.rapidviewmain.models;

import java.util.Comparator;

public class DetectionClassTypeComparator implements Comparator<Detection>{

	@Override
	public int compare(Detection c1, Detection c2) {
		
		return ClassType.valueOf(c2.getClasss().toUpperCase()).getPriority()-ClassType.valueOf(c1.getClasss().toUpperCase()).getPriority();
		
	}
	

}
