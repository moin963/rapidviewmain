package com.aiknights.com.rapidviewmain.models;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class DetectionChainedComparator implements Comparator<Detection>{
	
	
	private List<Comparator<Detection>> listComparators;
	
	

	@SafeVarargs
	public DetectionChainedComparator(Comparator<Detection>... comparators) {
		this.listComparators = Arrays.asList(comparators);
	}



	@Override
	public int compare(Detection o1, Detection o2) {
        for (Comparator<Detection> comparator : listComparators) {
            int result = comparator.compare(o1, o2);
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }
	
	}
