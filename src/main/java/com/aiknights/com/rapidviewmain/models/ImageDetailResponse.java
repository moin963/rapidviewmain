package com.aiknights.com.rapidviewmain.models;

import java.util.Comparator;
import java.util.List;


public class ImageDetailResponse {
	
	private Assessment assessment;
	private List<Detection> detections;
	
	public ImageDetailResponse() {
		super();
	}

	public ImageDetailResponse(Assessment assessment, List<Detection> detections) {
		super();
		this.assessment = assessment;
		this.detections = detections;
	}

	public Assessment getAssessment() {
		return assessment;
	}

	public void setAssessment(Assessment assessment) {
		this.assessment = assessment;
	}

	public List<Detection> getDetections() {
		return detections;
	}

	public void setDetections(List<Detection> detections) {
		this.detections = detections;
	}

	@Override
	public String toString() {
		return "ImageDetailResponse [assessment=" + assessment + ", detections=" + detections + "]";
	}
	
	
	
	

	
	

}
