package com.aiknights.com.rapidviewmain.models;

public class Resource {

	private int resouceId;
	private String resourceName;
	
	public Resource() {
		super();
	}

	public Resource(int resouceId, String resourceName) {
		super();
		this.resouceId = resouceId;
		this.resourceName = resourceName;
	}

	public int getResouceId() {
		return resouceId;
	}

	public void setResouceId(int resouceId) {
		this.resouceId = resouceId;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	@Override
	public String toString() {
		return "Resource [resouceId=" + resouceId + ", resourceName=" + resourceName + "]";
	}
	
	
}
