package com.aiknights.com.rapidviewmain.models;

public class ClaimNotification extends PolicyCheckNotification {
	
	private String claimId;
	private String claimantFirstName;
	private String claimantLastName;
	private String email;
	private String phoneNumber;
	private String address;
	private String city;
	private String state;
	private int zipCode;
	private String policyNumber;
	private String incidentType;
	private String incidentDate;
	private String incidentTime;
	private String claimDate;
	private int claimAmount;
	private ClaimStatus claimstatus;
	//need to be removed, Just placed for UI representation
	private int overAllRisk;
	
	public ClaimNotification() {
		super();
	}

	public String getClaimId() {
		return claimId;
	}

	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}

	public String getClaimantFirstName() {
		return claimantFirstName;
	}

	public void setClaimantFirstName(String claimantFirstName) {
		this.claimantFirstName = claimantFirstName;
	}

	public String getClaimantLastName() {
		return claimantLastName;
	}

	public void setClaimantLastName(String claimantLastName) {
		this.claimantLastName = claimantLastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getIncidentType() {
		return incidentType;
	}

	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}

	public String getIncidentDate() {
		return incidentDate;
	}

	public void setIncidentDate(String incidentDate) {
		this.incidentDate = incidentDate;
	}

	public String getIncidentTime() {
		return incidentTime;
	}

	public void setIncidentTime(String incidentTime) {
		this.incidentTime = incidentTime;
	}

	public String getClaimDate() {
		return claimDate;
	}

	public void setClaimDate(String claimDate) {
		this.claimDate = claimDate;
	}

	public ClaimStatus getClaimstatus() {
		return claimstatus;
	}

	public void setClaimstatus(ClaimStatus claimstatus) {
		this.claimstatus = claimstatus;
	}	

	public int getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(int claimAmount) {
		this.claimAmount = claimAmount;
	}

	public int getOverAllRisk() {
		return overAllRisk;
	}

	public void setOverAllRisk(int overAllRisk) {
		this.overAllRisk = overAllRisk;
	}

	@Override
	public String toString() {
		return "ClaimNotification [claimId=" + claimId + ", claimantFirstName=" + claimantFirstName
				+ ", claimantLastName=" + claimantLastName + ", email=" + email + ", phoneNumber=" + phoneNumber
				+ ", address=" + address + ", city=" + city + ", state=" + state + ", zipCode=" + zipCode
				+ ", policyNumber=" + policyNumber + ", incidentType=" + incidentType + ", incidentDate=" + incidentDate
				+ ", incidentTime=" + incidentTime + ", claimDate=" + claimDate + ", claimAmount=" + claimAmount
				+ ", claimstatus=" + claimstatus + ", overAllRisk=" + overAllRisk + "]";
	}

    

	
	
	
	

}
