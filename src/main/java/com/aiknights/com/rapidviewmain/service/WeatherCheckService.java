package com.aiknights.com.rapidviewmain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.WeatherRepository;
import com.aiknights.com.rapidviewmain.models.WeatherCheck;

@Service
public class WeatherCheckService {
	
	@Autowired
	private WeatherRepository weatherRepository;
	
	public WeatherCheck getWeatherCheckDataByClaimId(final String claimId) {
		return weatherRepository.getWeatherByClaimId(claimId);
	}
	

}
