package com.aiknights.com.rapidviewmain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.RoleRepository;

@Service
public class RoleService {


	@Autowired
	private RoleRepository roleRepository;
	
	public List<String> getRolesByUser(final String userName) {
		return roleRepository.getRolesByUsername(userName);
	}
	
}
