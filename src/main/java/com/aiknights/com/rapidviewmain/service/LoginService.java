package com.aiknights.com.rapidviewmain.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.UserDetailsRepository;
import com.aiknights.com.rapidviewmain.DAO.UserRepository;
import com.aiknights.com.rapidviewmain.DAOImpl.UserDAOImpl;
import com.aiknights.com.rapidviewmain.constants.RapidViewMainConstants;
import com.aiknights.com.rapidviewmain.models.User;
import com.aiknights.com.rapidviewmain.models.UserDetails;

@Service
public class LoginService {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginService.class);

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserDetailsRepository userDetailsRepository;
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	public ResponseEntity<Map<String, String>> isValidUser(final String userName, final String password) {
		final User userLoginDetails = userRepository.findUserByUserName(userName);
		final Map<String, String> responseMap = new HashMap<>();
		if(userLoginDetails == null) {
			responseMap.put(RapidViewMainConstants.USER_NAME, userName);
			responseMap.put(RapidViewMainConstants.STATUS, RapidViewMainConstants.USER_NOT_EXIST);
			return new ResponseEntity<>(responseMap,HttpStatus.NOT_FOUND);
		} else {
			if(passwordEncoder().matches(password, userLoginDetails.getPassword())) {
				final UserDetails userDetails = userDetailsRepository.findUserDetailsByUserName(userName);
				responseMap.put(RapidViewMainConstants.USER_NAME, userLoginDetails.getUsername());
				responseMap.put(RapidViewMainConstants.STATUS, RapidViewMainConstants.VALID);
				responseMap.put(RapidViewMainConstants.FIRST_NAME, userDetails.getFirstName());
				responseMap.put(RapidViewMainConstants.LAST_NAME, userDetails.getLastName());
				responseMap.put(RapidViewMainConstants.USER_ID, String.valueOf(userDetails.getUserId()));
				return new ResponseEntity<>(responseMap,HttpStatus.OK);
			} else {
				responseMap.put(RapidViewMainConstants.USER_NAME, userLoginDetails.getUsername());
				responseMap.put(RapidViewMainConstants.STATUS, RapidViewMainConstants.IN_VALID);
				return new ResponseEntity<>(responseMap,HttpStatus.FORBIDDEN);
			}
		}
	}
}
