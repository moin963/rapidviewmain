package com.aiknights.com.rapidviewmain.service;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.AbstractResource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.aiknights.com.rapidviewmain.DAO.ImageEvidenceRepository;
import com.aiknights.com.rapidviewmain.DAOImpl.ClaimNotificationDAOImpl;
import com.aiknights.com.rapidviewmain.constants.RapidViewMainConstants;
import com.aiknights.com.rapidviewmain.models.AnalyseReponseInDetail;
import com.aiknights.com.rapidviewmain.models.Assessment;
import com.aiknights.com.rapidviewmain.models.Base64DecodedMultipartFile;
import com.aiknights.com.rapidviewmain.models.ClassType;
import com.aiknights.com.rapidviewmain.models.DeleteIncidentImagesInput;
import com.aiknights.com.rapidviewmain.models.Detection;
import com.aiknights.com.rapidviewmain.models.DetectionChainedComparator;
import com.aiknights.com.rapidviewmain.models.DetectionClassTypeComparator;
import com.aiknights.com.rapidviewmain.models.DetectionScoreTypeComparator;
import com.aiknights.com.rapidviewmain.models.ImageDetailResponse;
import com.aiknights.com.rapidviewmain.models.ShingleType;
import com.aiknights.com.rapidviewmain.models.UploadImagesOutput;

@Service
public class UploadImageService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UploadImageService.class);
	
	@Autowired
	private ImageEvidenceRepository imageRepository;
	
	@Value("${upload.images.base-path}")
	private String baseDirectory;
	
	@Value("${upload.images.server-url}")
	private String imageServerUrl;
	
	public List<UploadImagesOutput> uploadImageFiles(final MultipartFile[] files,final String claimId,final String category,final String insuranceCompany,final String shingleType) {
		String s = createDirectories(Arrays.asList(insuranceCompany,claimId,category));
		List<UploadImagesOutput> uploadedResponse =  Arrays.asList(files).stream().map(file->uploadFile(file,s)).collect(Collectors.toList());
		saveUploadedImgeUrlInDB(uploadedResponse,claimId,shingleType);
		return uploadedResponse;
	}
	
	public AnalyseReponseInDetail analyseImage(final String imageId,final String url,final String claimId,final String category,final String insuranceCompany,final String shingleType) {
		try {
			AbstractResource resource = new UrlResource(url);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
			body.add("image", resource);
			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
			//String serverUrl = "http://23.127.132.57:5000/api/detect_image";
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<byte[]> response = restTemplate.postForEntity(RapidViewMainConstants.DETECT_IMAGE_URL, requestEntity, byte[].class);
			
			if(response.getStatusCodeValue()==200) {
			//create directories,store image generate url
			String pathToStore = createDirectories(Arrays.asList(insuranceCompany,claimId,category));
			Base64DecodedMultipartFile file = new Base64DecodedMultipartFile(response.getBody(), resource.getFilename());
			UploadImagesOutput output = uploadFile(file, pathToStore);
			System.out.println(output);
			LOGGER.info("Response received from detect image and stored file in file system and genrrated url: {}",output);
			//Now calling the image detail rest call
			return getImageAssessmentResults(imageId,url,RapidViewMainConstants.DETAIL_IMAGE_URL,output,requestEntity,claimId,shingleType);
			} else {
				LOGGER.error("Detect Image AI rest call  got failed");
				return null;
			}
		} catch (MalformedURLException | RestClientException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error occured while Rest client invocation or url formation: {}", e);
			e.printStackTrace();
			return null;
		}
		
	}
	
	public List<AnalyseReponseInDetail> getIncidentEvidenceForClaim(final String claimId) {
		return imageRepository.getIncidentEvidenceForClaim(claimId);
	}
	
	public int[] deleteIncidentEvidenceImages(final List<DeleteIncidentImagesInput> deleteImagesInput) {
		// TODO Need to delete images from File system also.
		int [] deletedArray = imageRepository.deleteIncidentEvidenceImages(deleteImagesInput);
		LOGGER.info("After deleting the ouutput retunred is: {}", deletedArray);
		return deletedArray;
	}
	
	private void deleteImagesFromServerFileSystem(final DeleteIncidentImagesInput deleteImagesInput) {
		File file = new File(deleteImagesInput.getSourceImageUrl());
		File file1 = new File(deleteImagesInput.getAnalysedImageUrl());
		
	}
	
	private String createDirectories(final List<String> names) {
		System.out.println(baseDirectory);
		 //String basePath = "C:\\photos\\";
		String basePath = baseDirectory;
		for(String s : names) {
			String folder = basePath+s;
			File directory = new File(folder);
			if(!directory.exists()) {
				directory.mkdir();
			}
			basePath = folder+File.separator;
		}
		System.out.println("Hi I am fine path "+basePath);
		return basePath;
	}
	
	private UploadImagesOutput uploadFile(MultipartFile file,String folder) {
		UploadImagesOutput outPut = new UploadImagesOutput();
		long uniqueIdForImage = System.currentTimeMillis();
		String uniqueId = String.valueOf(uniqueIdForImage)+'_'+file.getOriginalFilename();
		String finalPath = folder+uniqueId;
		try {
			byte[] bytes = file.getBytes();
			Path path = Paths.get(finalPath);
			Files.write(path, bytes);
			outPut.setUniqueIdForImage(uniqueIdForImage);
			outPut.setImageId(uniqueId);
			outPut.setImageUrl(prepareUrl(finalPath));
			outPut.setSourceImageName(file.getOriginalFilename());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(outPut);
		return outPut;
	}
	
	private String prepareUrl(final String path) {
		if(path!=null) {
			return path.replace("/opt/tomcat/webapps/", imageServerUrl);
		}
		return null;
	}
	
	private AnalyseReponseInDetail getImageAssessmentResults(final String imageId,final String imageURL,final String restApiUrl,final UploadImagesOutput output,HttpEntity<MultiValueMap<String, Object>> requestEntity,String claimId,final String shingleType) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ImageDetailResponse> response = restTemplate.postForEntity(restApiUrl, requestEntity, ImageDetailResponse.class);
		LOGGER.info("Detail Image AI rest call got response code: {}", response.getStatusCode());
		AnalyseReponseInDetail responseIndetail = prepareFinalResponseForAnalyse( response,output,imageURL,imageId,shingleType); 
		updateAnaysedImageDataToDB(responseIndetail);
		return responseIndetail;
	}

	private AnalyseReponseInDetail prepareFinalResponseForAnalyse(ResponseEntity<ImageDetailResponse> response, UploadImagesOutput output,String imageUrl,String imageId,String shingleType) {
		ImageDetailResponse imageDetailResponse = null;
		AnalyseReponseInDetail analyseReponseInDetail =null;
		long imageUniqueIdFromUI = Long.parseLong(imageId);
		if(response.getStatusCodeValue()==200) {
			System.out.println("I am success");
		   imageDetailResponse =  response.getBody();
			final Assessment assessment = imageDetailResponse.getAssessment();
			List<Detection> detectionList = imageDetailResponse.getDetections();
			//Ivan need to change 
			replaceNonSOTRM(detectionList);
			final Detection detection = takePriorityDetection(detectionList);
			
			analyseReponseInDetail = new AnalyseReponseInDetail();
			analyseReponseInDetail.setSourceImageId(imageUniqueIdFromUI);
			analyseReponseInDetail.setSourceImageUrl(imageUrl);
			analyseReponseInDetail.setAnalysedImageId(output.getImageId());
			analyseReponseInDetail.setAnalysedImageUrl(output.getImageUrl());
			analyseReponseInDetail.setAssessmentBlurrinessError((String) assessment.getBlurriness().get("error"));
			analyseReponseInDetail.setImageName(assessment.getName());
			analyseReponseInDetail.setAssessmentScaleError((String)assessment.getScale().get("error"));
			analyseReponseInDetail.setAssessmentStatus(assessment.getStatus());
			analyseReponseInDetail.setShingleType(shingleType);
			if(detection!=null) {
			  analyseReponseInDetail.setClassType(ClassType.valueOf(detection.getClasss().toUpperCase()));
			  analyseReponseInDetail.setDetectionScore(roundOfTheDoubleValue(detection.getScore()*100));
			  System.out.println("calculated detection score"+roundOfTheDoubleValue(detection.getScore()*100));
			  analyseReponseInDetail.setDamageSize(calculateDamageSize(detection.getDamageMaxShingleMinRatio(),shingleType));
			  System.out.println("calculated damage size"+calculateDamageSize(detection.getDamageMaxShingleMinRatio(),shingleType));
			
			}
			
			return analyseReponseInDetail;
	} else if(response.getStatusCodeValue()==420) {
		System.out.println("I am failure");
		imageDetailResponse = response.getBody();
	    final Assessment assessment = imageDetailResponse.getAssessment();
	    analyseReponseInDetail = new AnalyseReponseInDetail();
	    analyseReponseInDetail.setSourceImageId(imageUniqueIdFromUI);
		analyseReponseInDetail.setSourceImageUrl(imageUrl);
		analyseReponseInDetail.setAnalysedImageId(output.getImageId());
		analyseReponseInDetail.setAnalysedImageUrl(output.getImageUrl());
		analyseReponseInDetail.setAssessmentBlurrinessError((String) assessment.getBlurriness().get("error"));
		analyseReponseInDetail.setImageName(assessment.getName());
		analyseReponseInDetail.setAssessmentScaleError((String)assessment.getScale().get("error"));
		analyseReponseInDetail.setAssessmentStatus(assessment.getStatus());
		analyseReponseInDetail.setShingleType(shingleType);
		return analyseReponseInDetail;
	} else {
		LOGGER.info("Detail Image Rest call failed:");
		return new AnalyseReponseInDetail();
	}
}
	private Detection takePriorityDetection(final List<Detection> detectionList) {
		if(!detectionList.isEmpty()) {
		  Collections.sort(detectionList, new DetectionChainedComparator(
				  
				  new DetectionClassTypeComparator(),
				  new DetectionScoreTypeComparator()));
		return detectionList.get(0);
		} else {
			return null;
		}
		
	}
	
	private void replaceNonSOTRM(final List<Detection> list) {
		for(Detection d: list) {
			if("Non-Storm".equalsIgnoreCase(d.getClasss())) {
				d.setClasss("NON_STORM");
			}
		}
	}
	
	private void saveUploadedImgeUrlInDB(final List<UploadImagesOutput> uploadedResponse,final String claimId,final String shingleType) {
		List<AnalyseReponseInDetail> analyseReponseInDetail = new ArrayList<>();
		for(UploadImagesOutput o : uploadedResponse) {
			//setting shingle type to send back in response;
			o.setShingletype(shingleType);
			AnalyseReponseInDetail reponse = new AnalyseReponseInDetail(o.getUniqueIdForImage(),o.getImageUrl(),o.getSourceImageName());
			reponse.setShingleType(shingleType);
			analyseReponseInDetail.add(reponse);
		}
		int[] uploadedArray = imageRepository.saveUploadedImages(analyseReponseInDetail,claimId);
		LOGGER.info("inserted the uploaded images and got output: {}",uploadedArray);
		
	}
	
	private void updateAnaysedImageDataToDB(final AnalyseReponseInDetail analyseReponseInDetail) {
		int response = imageRepository.updateAnalysedImageDatatoDB(analyseReponseInDetail);
		LOGGER.info("updated analysed image data to db: {}", response);
	}
	
	private double roundOfTheDoubleValue(Double score) {
		Double d = new Double(score);
		int intScore = d.intValue();
		double doubleScore = intScore;
		return doubleScore;
	}
	
	
	private double calculateDamageSize(Double damageMaxShingleMinRatio,final String shingleType)
	{
		ShingleType shingle = ShingleType.valueOf(shingleType);
		double shingleValue = shingle.getShingleValue();
		LOGGER.info("DamageMaxShingleMinRatio is: {}",damageMaxShingleMinRatio);
		double damageSize=-1.0;
		if(damageMaxShingleMinRatio!=null) {
		 damageSize = (damageMaxShingleMinRatio!=-1.0)?(damageMaxShingleMinRatio*shingleValue):-1.0;
		} 
		DecimalFormat df = new DecimalFormat("#.##");
		damageSize = Double.valueOf(df.format(damageSize));
		return damageSize;
	}
}