package com.aiknights.com.rapidviewmain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.UserDetailsRepository;
import com.aiknights.com.rapidviewmain.models.UserDetails;

@Service
public class UserDetailsService {
	
	@Autowired
	private UserDetailsRepository userRepository;

	public UserDetails getUserDetailsByUserName(final String userName) {
		return userRepository.findUserDetailsByUserName(userName);
	}
}
