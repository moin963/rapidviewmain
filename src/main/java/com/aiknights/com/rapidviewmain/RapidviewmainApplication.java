package com.aiknights.com.rapidviewmain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class RapidviewmainApplication  extends SpringBootServletInitializer{
	
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(RapidviewmainApplication.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(RapidviewmainApplication.class, args);
	}

}