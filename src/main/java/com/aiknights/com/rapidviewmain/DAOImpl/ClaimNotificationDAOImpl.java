package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.ClaimNotificationRepository;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;

@Repository
public class ClaimNotificationDAOImpl implements ClaimNotificationRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ClaimNotificationDAOImpl.class);
	
	@Override
	public ClaimNotification getClaimByClaimId(final String claimId) {
		final String GET_CLAIM_BY_CLAIMID = "select * from claim_notification where claim_id= ?;";
		try {
			final ClaimNotification claimNotification = jdbcTemplate.queryForObject(GET_CLAIM_BY_CLAIMID, new ClaimNotificationRowMapper(),claimId );
			LOGGER.info("ClaimNotification for a given claim id is: {}",claimNotification);
			return claimNotification;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public List<ClaimNotification> getAssignedClaimsForUser(final int userId) {
		 final String GET_CLAIMS_BY_USER_ID = "select * from claim_notification where claim_id IN(select claim_id from adjuster_claim where adjuster_id = ?);";
		 List<ClaimNotification> claimList = jdbcTemplate.query(GET_CLAIMS_BY_USER_ID, new Object[] {userId}, new ClaimNotificationRowMapper());
		 LOGGER.info("Claims for a user are: {}", claimList);
		 return claimList;
	}

	@Override
	public ClaimNotification getClaimWithPolicyDataByClaimId(String claimId) {
		final String GET_CLAIM_POLICY_DATE_BY_CLAIM_ID="select * from claim_notification INNER  JOIN policy_check  ON  claim_notification.policy_number = policy_check.policy_number where claim_notification.claim_id=?;";
		try {
			final ClaimNotification claimNotification = jdbcTemplate.queryForObject(GET_CLAIM_POLICY_DATE_BY_CLAIM_ID, new ClaimPolicyNotificationRowMapper(),claimId );
			LOGGER.info("ClaimNotification for a given claim id is: {}",claimNotification);
			return claimNotification;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

}
