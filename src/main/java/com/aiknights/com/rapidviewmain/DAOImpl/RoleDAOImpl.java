package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.RoleRepository;
import com.aiknights.com.rapidviewmain.models.Role;

@Repository
public class RoleDAOImpl implements RoleRepository{

	
	private static final Logger logger = LoggerFactory.getLogger(RoleDAOImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	@Override
	public Role getRoleByRoleId(final int roleId) {
		// TODO not used now   
		return null;
	}

	@Override
	public List<String> getRolesByUsername(final String userName) {
		final String GET_ROLES_BY_USER_NAME = "select rolename from role where username = ?";
		 List<String> rolesList = new ArrayList<>();
		 final List<String> roles = jdbcTemplate.queryForList(GET_ROLES_BY_USER_NAME, new Object[]{userName}, String.class);
		 logger.info("For a user: {} roles are: {}", userName,roles);
		 rolesList.addAll(roles);
		 return rolesList;    
	}
}
