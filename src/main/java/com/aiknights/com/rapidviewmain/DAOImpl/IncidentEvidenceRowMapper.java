package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.AnalyseReponseInDetail;
import com.aiknights.com.rapidviewmain.models.ClassType;

public class IncidentEvidenceRowMapper implements RowMapper<AnalyseReponseInDetail>{

	@Override
	public AnalyseReponseInDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		AnalyseReponseInDetail analyseReponseInDetail = new AnalyseReponseInDetail();
		analyseReponseInDetail.setSourceImageId(rs.getLong("imageid"));
		analyseReponseInDetail.setSourceImageUrl(rs.getString("source_image_url"));
		analyseReponseInDetail.setAnalysedImageUrl(rs.getString("analysed_image_url"));
		analyseReponseInDetail.setAnalysedImageId(rs.getString("analysed_imageid"));
		analyseReponseInDetail.setAssessmentBlurrinessError(rs.getString("assessment_blurry_error"));
		analyseReponseInDetail.setImageName(rs.getString("imageName"));
		analyseReponseInDetail.setAssessmentScaleError(rs.getString("assessment_scale_error"));
		analyseReponseInDetail.setAssessmentStatus(rs.getString("assessment_status"));
		String classType = rs.getString("class_type")!=null?rs.getString("class_type"):"NON_CONCLUDED";
		analyseReponseInDetail.setClassType(ClassType.valueOf(classType));
		analyseReponseInDetail.setDetectionScore(rs.getDouble("detection_score"));
		analyseReponseInDetail.setDamageSize(rs.getDouble("damage_size"));
		analyseReponseInDetail.setShingleType(rs.getString("shingle_type"));
		analyseReponseInDetail.setSourceImageName(rs.getString("source_image_name"));
		return analyseReponseInDetail;
	}

}
