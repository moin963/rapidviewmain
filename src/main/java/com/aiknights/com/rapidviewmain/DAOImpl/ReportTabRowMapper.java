package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.dto.ReportGenerationDTO;
import com.aiknights.com.rapidviewmain.dto.ReportTabDTO;

public class ReportTabRowMapper implements RowMapper<ReportTabDTO>{

	@Override
	public ReportTabDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReportTabDTO reportDTO = new ReportTabDTO();
		
		reportDTO.setPolicyCoverage(rs.getInt("policy_coverage"));
		
		reportDTO.setDudictable(rs.getInt("deductible"));
		
		reportDTO.setClaimAmount(rs.getInt("claim_amount"));
		
		reportDTO.setPayoutAmount(rs.getInt("payout_amount"));
		
		reportDTO.setWeatherAssesment(rs.getDouble("weather_assesment"));
		
		reportDTO.setIncidentEvidenceAnalysis(rs.getDouble("incident_evidence_assesment"));
		
		reportDTO.setOverallRisk(rs.getDouble("overall_risk"));
		
		
		
		
		
		
		
		/*
		 * claimNotification.setClaimId(rs.getString("claim_id"));
		 * claimNotification.setClaimantFirstName(rs.getString("firstname"));
		 * claimNotification.setClaimantLastName(rs.getString("lastname"));
		 * claimNotification.setEmail(rs.getString("email"));
		 * claimNotification.setPhoneNumber(rs.getString("phone_number"));
		 * claimNotification.setAddress(rs.getString("street_address"));
		 * claimNotification.setCity(rs.getString("city"));
		 * claimNotification.setState(rs.getString("state"));
		 * claimNotification.setZipCode(rs.getInt("zipcode"));
		 * claimNotification.setPolicyNumber(rs.getString("policy_number"));
		 * claimNotification.setIncidentId(rs.getInt("incident_id"));
		 * claimNotification.setClaimDate(rs.getDate("claim_date").toString());
		 * claimNotification.setClaimstatus(ClaimStatus.valueOf(rs.getString("status")))
		 * ;
		 */
		return reportDTO;
	}
	
	

}
