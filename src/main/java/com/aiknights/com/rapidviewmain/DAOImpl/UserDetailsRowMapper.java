package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.User;
import com.aiknights.com.rapidviewmain.models.UserDetails;

public class UserDetailsRowMapper implements RowMapper<UserDetails>{

	@Override
	public UserDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserDetails userDetais = new UserDetails();
		userDetais.setUserId(rs.getInt("userid"));
		userDetais.setUserName(rs.getString("username"));
		userDetais.setFirstName(rs.getString("firstname"));
		userDetais.setLastName(rs.getString("lastname"));
		userDetais.setEmail(rs.getString("email"));
		userDetais.setCity(rs.getString("city"));
		return userDetais;
	}

}
