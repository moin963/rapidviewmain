package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.ResourceRepository;
import com.aiknights.com.rapidviewmain.models.Resource;

@Repository
public class ResourceDAOImpl implements ResourceRepository{
	

	private static final Logger logger = LoggerFactory.getLogger(ResourceDAOImpl.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Resource getResourceById(int resourceId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getResourcesByUserName(final String userName) {
		final String GET_RESOURCES_BY_USER_NAME = "select resourcename from resources where username = ?";
		 List<String> resourceList = new ArrayList<>();
		 final List<String> resources = jdbcTemplate.queryForList(GET_RESOURCES_BY_USER_NAME, new Object[]{userName}, String.class);
		 logger.info("For a user: {} resources are: {}", userName,resources);
		 resourceList.addAll(resources);
		 return resourceList;  
	}

}
