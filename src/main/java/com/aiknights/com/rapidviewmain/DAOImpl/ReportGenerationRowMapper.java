package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.dto.ReportGenerationDTO;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;
import com.aiknights.com.rapidviewmain.models.ClaimStatus;

public class ReportGenerationRowMapper implements RowMapper<ReportGenerationDTO>{

	@Override
	public ReportGenerationDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReportGenerationDTO reportDTO = new ReportGenerationDTO();
		
		reportDTO.setClaimantName(rs.getString("firstname"));
		reportDTO.setPolicyNumber(rs.getString("policy_number"));
		reportDTO.setAddress(rs.getString("street_address"));
		reportDTO.setInsuranceCompany(rs.getString("insurance_company"));
		reportDTO.setClaimNumber(rs.getString("claim_id"));
		reportDTO.setTypeOfClaim(rs.getString("incident_type"));
		reportDTO.setIncidentDate(rs.getDate("incident_date"));
		reportDTO.setIncidentTime(rs.getDate("incident_time"));
		reportDTO.setDudictable(rs.getInt("deductible"));
		reportDTO.setPayoutAmount(rs.getString("payout_amount"));
		reportDTO.setDateClosed(rs.getString("date_submit"));
		
		
		
		
		
		/*
		 * claimNotification.setClaimId(rs.getString("claim_id"));
		 * claimNotification.setClaimantFirstName(rs.getString("firstname"));
		 * claimNotification.setClaimantLastName(rs.getString("lastname"));
		 * claimNotification.setEmail(rs.getString("email"));
		 * claimNotification.setPhoneNumber(rs.getString("phone_number"));
		 * claimNotification.setAddress(rs.getString("street_address"));
		 * claimNotification.setCity(rs.getString("city"));
		 * claimNotification.setState(rs.getString("state"));
		 * claimNotification.setZipCode(rs.getInt("zipcode"));
		 * claimNotification.setPolicyNumber(rs.getString("policy_number"));
		 * claimNotification.setIncidentId(rs.getInt("incident_id"));
		 * claimNotification.setClaimDate(rs.getDate("claim_date").toString());
		 * claimNotification.setClaimstatus(ClaimStatus.valueOf(rs.getString("status")))
		 * ;
		 */
		return reportDTO;
	}
	
	

}
