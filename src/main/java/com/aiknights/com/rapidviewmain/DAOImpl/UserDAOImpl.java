package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.UserRepository;
import com.aiknights.com.rapidviewmain.models.User;

@Repository
public class UserDAOImpl implements UserRepository{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	@Override
	public User findUserByUserName(String userName) {
		
		final String GET_USER_BY_USERNAME = "select * from users where username=? and enabled = true";
		try {
			User userLoginDetails = jdbcTemplate.queryForObject(GET_USER_BY_USERNAME, new UserRowMapper(), userName);
			logger.info("The user with username is: {}", userLoginDetails);
			final List<String> roles = getRolesByUsername(userName);
			logger.info("username: {} has the roles: {}", userName, roles);
			userLoginDetails.getRoles().addAll(roles);
			return userLoginDetails;
		} catch (EmptyResultDataAccessException  e) {
			return null;
		}
	}
	
	private List<String> getRolesByUsername(final String userName) {
		final String GET_ROLES_BY_USER_NAME = "select rolename from role where username = ?";
		 List<String> rolesList = new ArrayList<>();
		 final List<String> roles = jdbcTemplate.queryForList(GET_ROLES_BY_USER_NAME, new Object[]{userName}, String.class);
		 logger.info("For a user: {} roles are: {}", userName,roles);
		 rolesList.addAll(roles);
		 return rolesList;    
	}
	
	
}
