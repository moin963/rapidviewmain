package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.UserDetailsRepository;

import com.aiknights.com.rapidviewmain.models.UserDetails;

@Repository
public class UserDetailsDAOImpl implements UserDetailsRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(UserDetailsDAOImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public UserDetails findUserDetailsByUserName(final String userName) {
		//UserDetails userDetais = null;
		final String GET_USERDETAILS_BY_USERNAME = "select * from user_details where username=?";
		try {
			UserDetails userDetais  = jdbcTemplate.queryForObject(GET_USERDETAILS_BY_USERNAME, new UserDetailsRowMapper(), userName);
			logger.info("The userDetails with username is: {}", userDetais);
			return userDetais;
		} catch (EmptyResultDataAccessException  e) {
			return null;
		}
	}

}
