package com.aiknights.com.rapidviewmain.constants;

public final class RapidViewMainConstants {
	
	private RapidViewMainConstants() {
		
	}
	
	public static final String USER_NAME = "userName";
	public static final String STATUS = "status";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String VALID = "valid";
	public static final String IN_VALID = "invalid";
	public static final String USER_NOT_EXIST = "user not exist";
	public static final String ROLE = "role";
	public static final String USER_ID = "userid";
	public static final String DETECT_IMAGE_URL = "http://23.127.132.57:5000/api/detect_image";
	public static final String DETAIL_IMAGE_URL = "http://23.127.132.57:5000/api/detect_detail";
	

}
