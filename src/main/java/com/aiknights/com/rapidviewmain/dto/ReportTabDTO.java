package com.aiknights.com.rapidviewmain.dto;

public class ReportTabDTO {
	
	private Integer policyCoverage;
	
	private Integer dudictable;
	
	private Integer claimAmount;
	
	private Integer payoutAmount;
	
	private Double weatherAssesment;
	
	private Double incidentEvidenceAnalysis;
	
	private Double overallRisk;

	public Integer getPolicyCoverage() {
		return policyCoverage;
	}

	public void setPolicyCoverage(Integer policyCoverage) {
		this.policyCoverage = policyCoverage;
	}

	public Integer getDudictable() {
		return dudictable;
	}

	public void setDudictable(Integer dudictable) {
		this.dudictable = dudictable;
	}

	public Integer getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(Integer claimAmount) {
		this.claimAmount = claimAmount;
	}

	public Integer getPayoutAmount() {
		return payoutAmount;
	}

	public void setPayoutAmount(Integer payoutAmount) {
		this.payoutAmount = payoutAmount;
	}

	public Double getWeatherAssesment() {
		return weatherAssesment;
	}

	public void setWeatherAssesment(Double weatherAssesment) {
		this.weatherAssesment = weatherAssesment;
	}

	public Double getIncidentEvidenceAnalysis() {
		return incidentEvidenceAnalysis;
	}

	public void setIncidentEvidenceAnalysis(Double incidentEvidenceAnalysis) {
		this.incidentEvidenceAnalysis = incidentEvidenceAnalysis;
	}

	public Double getOverallRisk() {
		return overallRisk;
	}

	public void setOverallRisk(Double overallRisk) {
		this.overallRisk = overallRisk;
	}

	@Override
	public String toString() {
		return "ReportTabDTO [policyCoverage=" + policyCoverage + ", dudictable=" + dudictable + ", claimAmount="
				+ claimAmount + ", payoutAmount=" + payoutAmount + ", weatherAssesment=" + weatherAssesment
				+ ", incidentEvidenceAnalysis=" + incidentEvidenceAnalysis + ", overallRisk=" + overallRisk + "]";
	}
	
	
	

}
