package com.aiknights.com.rapidviewmain.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.constants.RapidViewMainConstants;
import com.aiknights.com.rapidviewmain.service.LoginService;
import com.aiknights.com.rapidviewmain.service.ResourceService;
import com.aiknights.com.rapidviewmain.service.RoleService;

@RequestMapping("/api")
@RestController
public class UserController {
	
	@Autowired
	private ResourceService resourceService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private LoginService loginService;
	

	@GetMapping("/all")
	public String hello() {
		return "hello aiknights";
	}
	
	@PreAuthorize("hasAnyRole('ADMIN','ADJUSTER')")
	@GetMapping("/secured/all")
	public String securedAl() {
		return "hello secured aiknights";
	}
	
	//@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping(value = "/resourcesForUser/{userName}")
	public ResponseEntity<Map<String, List<String>>> getResourcesByUser(@PathVariable final String userName){
		final Map<String,List<String>> resourcesMap = new HashMap<>();
		final List<String> resourceList = resourceService.getResourceByUser(userName);
		resourcesMap.put("resources",resourceList);
		return new ResponseEntity<>(resourcesMap,HttpStatus.OK);
	}
	
	//@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping(value = "/rolesForUser/{userName}")
	public ResponseEntity<Map<String, List<String>>> getRolesByUser(@PathVariable final String userName) {
		final Map<String,List<String>> rolesMap = new HashMap<>();
		final List<String> rolesList = roleService.getRolesByUser(userName);
		rolesMap.put(RapidViewMainConstants.ROLE, rolesList);
		return new ResponseEntity<>(rolesMap,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/loginUser", method = RequestMethod.POST )
	public ResponseEntity<Map<String, String>> validteUser(@RequestBody final Map<String, String> userDetails) {
		final String userName = userDetails.get("userName");
		final String password = userDetails.get("password");
		return loginService.isValidUser(userName, password);
	}
	
}
