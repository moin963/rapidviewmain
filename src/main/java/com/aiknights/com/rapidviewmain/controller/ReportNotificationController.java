package com.aiknights.com.rapidviewmain.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.dto.ReportTabDTO;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;
import com.aiknights.com.rapidviewmain.service.ClaimNotificationService;
import com.aiknights.com.rapidviewmain.service.ReportGenerationService;

@RequestMapping("/api")
@RestController
public class ReportNotificationController {

	@Autowired
	private ClaimNotificationService claimService;
	
	@Autowired
	private ReportGenerationService reportGenerationService;
	
	
	
	@GetMapping("/getReport/{claimId}")
	public ResponseEntity<Map<String, ReportTabDTO>> getReportTabForClaimId(@PathVariable final String claimId){
		Map<String,ReportTabDTO> reportTabMap = reportGenerationService.getReportTabInfoUsingClaimId(claimId);
		return new ResponseEntity<>(reportTabMap,HttpStatus.OK);
	}
}
