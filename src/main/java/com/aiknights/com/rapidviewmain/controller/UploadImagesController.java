package com.aiknights.com.rapidviewmain.controller;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.AbstractResource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.aiknights.com.rapidviewmain.DAOImpl.IncidentEvidenceRowMapper;
import com.aiknights.com.rapidviewmain.models.AnalyseReponseInDetail;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;
import com.aiknights.com.rapidviewmain.models.DeleteIncidentImagesInput;
import com.aiknights.com.rapidviewmain.models.UploadImagesOutput;
import com.aiknights.com.rapidviewmain.service.UploadImageService;

@RequestMapping("/api")
@RestController
public class UploadImagesController {
	
	@Autowired
	private UploadImageService uploadService;
	
	
	@PostMapping("/uploadImageFiles")
	public ResponseEntity<Map<String, List<UploadImagesOutput>>> uploadFiles(@RequestParam("files") MultipartFile[] files, 
																			 @RequestParam("claimId") String claimId,
																			 @RequestParam("category") String category,
																			 @RequestParam("insuranceCompany") String insuranceCompany,
																			 @RequestParam("shingleType") String shingleType) {
		List<UploadImagesOutput> uploadedList = uploadService.uploadImageFiles(files, claimId, category, insuranceCompany,shingleType);
		Map<String,List<UploadImagesOutput>> uploadedMap = new HashMap<>();
		uploadedMap.put("uploadedMap",uploadedList);
		return new ResponseEntity<>(uploadedMap,HttpStatus.OK);
	}
	
	@PostMapping(value = "/analyseImage"/* ,produces = MediaType.IMAGE_JPEG_VALUE */)
	public ResponseEntity<?> analyseImage(@RequestParam("imageId") final String imageId,
										  @RequestParam("url") final String url,
										  @RequestParam("claimId") final String claimId,
										  @RequestParam("category") final String category,
										  @RequestParam("insuranceCompany") final String insuranceCompany,
										  @RequestParam("shingleType") final String shingleType) {
		AnalyseReponseInDetail analysedOutput = uploadService.analyseImage(imageId,url, claimId, category, insuranceCompany,shingleType);
		Map<String,AnalyseReponseInDetail> analysedMap = new HashMap<>();
		analysedMap.put("analysedMap",analysedOutput);
		if(analysedOutput!=null) {
			return new ResponseEntity<Map<String, AnalyseReponseInDetail>>(analysedMap,HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Analysis Rest call failed. It may be due to special characters andspaces in image name or AI service not responding.",HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@GetMapping(value="/getIncidentEvidenceForClaim/{claimId}")
	public  ResponseEntity<Map<String, List<AnalyseReponseInDetail>>> getIncidentEvidenceForClaim(@PathVariable final String claimId) {
		List<AnalyseReponseInDetail> imageList = uploadService.getIncidentEvidenceForClaim(claimId);
		Map<String,List<AnalyseReponseInDetail>> imagesMap = new HashMap<>();
		imagesMap.put("imageList",imageList);
		return new ResponseEntity<>(imagesMap,HttpStatus.OK);
		
	}
	
	@PostMapping(value = "/deleteIncidentEvidenceImages")
	public ResponseEntity<int[]> deleteIncidentEvidenceImages(@RequestBody List<DeleteIncidentImagesInput> deleteIncidentImages){
		int [] deletedArray = uploadService.deleteIncidentEvidenceImages(deleteIncidentImages);
		//return  uploadService.deleteIncidentEvidenceImages(deleteIncidentImages);
		return new ResponseEntity<>(deletedArray, HttpStatus.OK);
		
	}

}