package com.aiknights.com.rapidviewmain.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.models.ClaimNotification;
import com.aiknights.com.rapidviewmain.models.WeatherCheck;
import com.aiknights.com.rapidviewmain.service.ClaimNotificationService;

@RequestMapping("/api")
@RestController
public class ClaimNotificationController {

	@Autowired
	private ClaimNotificationService claimService;
	
	@GetMapping("/getAssignedClaims/{userId}")
	public ResponseEntity<Map<String, List<ClaimNotification>>> getClaimsAssignedForUser(@PathVariable final int userId){
		final Map<String, List<ClaimNotification>> responseMap = new HashMap<>();
		final List<ClaimNotification> claimList = claimService.getAssigedClaimsForUser(userId);
		responseMap.put("claimList", claimList);
		return new ResponseEntity<>(responseMap,HttpStatus.OK);
	}
	
	@GetMapping("/getClaimById/{claimId}")
	public ResponseEntity<Map<String, ClaimNotification>> getClaimNotificationByClaimId(@PathVariable final String claimId) {
		final Map<String,ClaimNotification> claimMap = new HashMap<>();
		final ClaimNotification  claim = claimService.getClaimByClaimId(claimId);
		if(claim == null) {
			claimMap.put("claim",null);
			return new ResponseEntity<>(claimMap,HttpStatus.NOT_FOUND);
		} else {
			claimMap.put("claim",claim);
			return new ResponseEntity<>(claimMap,HttpStatus.OK);
		}
	}
	
	@GetMapping("/getClaimPolicy/{claimId}")
	public ResponseEntity<Map<String, ClaimNotification>> getClaimWithPolicyDataByClaimId(@PathVariable final String claimId) {
		final Map<String,ClaimNotification> claimMap = new HashMap<>();
		final ClaimNotification  claim = claimService.getClaimWithPolicyDataByClaimId(claimId);
		if(claim == null) {
			claimMap.put("claim",null);
			return new ResponseEntity<>(claimMap,HttpStatus.NOT_FOUND);
		} else {
			claimMap.put("claim",claim);
			return new ResponseEntity<>(claimMap,HttpStatus.OK);
		}
	}
	
}
