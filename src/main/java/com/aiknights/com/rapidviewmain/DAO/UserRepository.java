package com.aiknights.com.rapidviewmain.DAO;

import com.aiknights.com.rapidviewmain.models.User;

public interface UserRepository {

	User findUserByUserName(final String userName);
}
