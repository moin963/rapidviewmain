package com.aiknights.com.rapidviewmain.DAO;

import com.aiknights.com.rapidviewmain.models.WeatherCheck;

public interface WeatherRepository {
	
	WeatherCheck getWeatherById(final int wetherId);
	
	WeatherCheck getWeatherByClaimId(final String claimid);
	
}
