package com.aiknights.com.rapidviewmain.DAO;



import com.aiknights.com.rapidviewmain.models.UserDetails;

public interface UserDetailsRepository {

	UserDetails findUserDetailsByUserName(final String userName);
	
}
