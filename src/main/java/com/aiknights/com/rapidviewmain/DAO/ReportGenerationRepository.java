package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import com.aiknights.com.rapidviewmain.dto.ReportTabDTO;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;

public interface ReportGenerationRepository {
	
	
	ReportTabDTO getReportDetails(final String claimId);
	

}
